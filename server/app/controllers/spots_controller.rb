class SpotsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  
  def check_in
    if request.post?
      SpotClues.new.check_in(params[:spot_id])
      render :text => "ok"
    end
  end
  
  def play
    render :xml => SpotClues.new.play(params[:spot_id])
  end
  
  def play_last
    render :xml => SpotClues.new.play(SpotSettings.current_spot)
  end
  
  def play_scary_call
    render :xml => SpotClues.new.play_scary_call
  end
end
