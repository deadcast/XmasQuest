module ApplicationHelper
  def percent_of_quest_complete
    number_to_percentage(12.5 * (SpotSettings.current_spot.to_i-1), :precision => 0)
  end
end
