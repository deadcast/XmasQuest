class SpotClues
  def check_in(number)
    self.send("check_in_#{number}")
    SpotSettings.current_spot = number
  end
  
  def play(number)
    self.send("play_clue_#{number}")
  end
  
  # Spot 1 - Outside home
  def check_in_1
    async_make_call(Phone::ALL, 1)
  end
  
  def play_clue_1
    say("Hello, and welcome to the christmas quest. Your first stop is to go to the star bucks on coffee and get a drink. From there you will receive further instructions.")
  end
  
  # Spot 2 - Bass Pro
  def check_in_2
    async_make_call(Phone::BRUCE, 2)
  end
  
  def play_clue_2
    say("Hello Bruce, you made it to star bucks. Please open envelope 1. Once you have your drinks, travel to a place in man tea ka where you can buy things for the outdoors. This store is really, really big!")
  end
  
  # Spot 3 - Chilaberries
  def check_in_3
    async_send_sms(Phone::BRUCE, 8)
    async_make_call(Phone::ALL, 3)
  end
  
  def play_clue_3
    say("Hello everyone, you have successfully made it to Bass Pro! Please open envelope 2. When you're done shopping at Bass Pro, go to the ice cream store with the name berries in it. It should be near by.")
  end
  
  # Spot 4 - Best Buy
  def check_in_4
    async_make_call(Phone::CORY, 4)
  end
  
  def play_clue_4
    say("Hello Cory, you made it to chilaberries. Please open envelope 3. When everyone has gotten something to eat, go to the biggest electronic store in modesto located on sisk road with the best buys in town.")
  end
  
  # Spot 5 - Coach
  def check_in_5
    async_send_sms(Phone::CORY, 2)
    async_make_call(Phone::MICHAELA, 5)
  end
  
  def play_clue_5
    say("Hello Michaela, you made it to best buy. Please open envelope 4. Once Cory is done shopping, take a ride to coach to claim your prize.")
  end
  
  # Spot 6 - McHenry Village
  def check_in_6
    async_send_sms(Phone::MICHAELA, 5)
    async_make_call(Phone::SUSAN, 6)
  end
  
  def play_clue_6
    say("Hello Susan, you made it to Coach. Good job. You may now open envelope 5. When Michaela is done shopping, go to the village and visit the store that starts with a kay.")
  end
  
  # Spot 7 - Back home
  def check_in_7
    async_send_sms(Phone::SUSAN, 3)
    async_make_call(Phone::ALL, 7)
  end
  
  def play_clue_7
    say("Hello everyone, you made it to Keller's! Please open envelope 6. Once you're done shopping at Keller's, return home for further instructions.")
  end
  
  # Spot 8 - Welcome home and secret code order
  def check_in_8
    async_make_call(Phone::ALL, 8)
    Delayed::Job.enqueue Delayed::PerformableMethod.new(self, :scary_call, nil), 1, 10.minutes.from_now
  end
  
  def play_clue_8
    say("Good, you made it back home. The number you each received via text message is your secret code to unlocking the safe. The order is cory, susan, michaela and bruce. In a few minutes you will be receiving a call on the home phone. Be ready to answer. Also, don't touch or unplug the christmas quest box.")
  end
  
  # Spot 9 - At Home
  def check_in_9
    async_make_call(Phone::ALL, 9)
  end
  
  def play_clue_9
    say("Congratulations! You have successfully finished your quest. Now go in and have lunch. Good bye.")
  end
  
  # The scary call!
  def scary_call(arg)
    connect_to_twilio
    Twilio::Call.make(::TWILIO_NUMBER, Phone::HOME, "http://xmas-quest.heroku.com/play_scary_call")
  end
  
  def play_scary_call
    say("Hello. Now go to the place where cory used to take cooking classes. This is your final stop.")
  end
  
  private
  
  def say(message)
   builder = Nokogiri::XML::Builder.new do |q|
      q.Response {
        q.Say(message)
      }
    end
    builder.to_xml
  end
  
  def connect_to_twilio
    Twilio.connect(::TWILIO_SID, ::TWILIO_TOKEN)
  end
  
  def async_make_call(number, spot_number)
    self.send_later(:make_call, number, spot_number)
  end
  
  def make_call(number, spot_number)
    connect_to_twilio
    if number == Phone::ALL
      call([Phone::BRUCE, Phone::SUSAN, Phone::CORY, Phone::MICHAELA], spot_number)
    else
      call(number, spot_number)
    end
  end
  
  def call(number, spot_number)
    if number.is_a?(Array)
      # Use recursion to call all of the phone numbers
      call(number.pop, spot_number)
      call(number, spot_number) unless number.empty?
    else
      Twilio::Call.make(::TWILIO_NUMBER, number, "http://xmas-quest.heroku.com/spots/#{spot_number}/play")
    end
  end
  
  def async_send_sms(number, secret)
    self.send_later(:send_sms, number, secret)
  end
  
  def send_sms(number, secret)
    connect_to_twilio
    sms(number, "Here's your secret code: #{secret}. You have only 20 minutes to shop here. We are watching.")
  end
  
  def sms(number, message)
    Twilio::Sms.message(::TWILIO_NUMBER, number, message)
  end
end