//
//  XmasQuestAppDelegate.h
//  XmasQuest
//
//  Created by caleb cohoon on 11/26/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface XmasQuestAppDelegate : NSObject <UIApplicationDelegate, CLLocationManagerDelegate> {
    UIWindow *window;
	IBOutlet UILabel *status;
	IBOutlet UILabel *location;
	IBOutlet UILabel *distanceToTarget;
	IBOutlet UILabel *currentTarget;
	IBOutlet UILabel *checkInResponse;
	IBOutlet UILabel *accuracy;
	
	CLLocationManager *locationManager;
	NSURLConnection *connectionInProgress;
	NSArray *spots;
	NSArray *spot_radii;
	NSMutableData *responseData;
	
	int currentSpot;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

- (void)checkInAtSpot;
- (void)resetConnection;
- (void)startQuest;
- (IBAction)startQuestManually;

@end

