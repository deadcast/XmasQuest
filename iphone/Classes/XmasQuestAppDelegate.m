//
//  XmasQuestAppDelegate.m
//  XmasQuest
//
//  Created by caleb cohoon on 11/26/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "XmasQuestAppDelegate.h"

@implementation XmasQuestAppDelegate

@synthesize window;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
	
	// Make sure the app doesn't get sleepy.
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	
	[[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(batteryStateDidChange:)
												 name:UIDeviceBatteryStateDidChangeNotification
													object:nil];
	
	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDelegate:self];
	[locationManager setDistanceFilter:kCLDistanceFilterNone];
	[locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
	
	spots = [[NSArray alloc] initWithObjects:
			 [[CLLocation alloc] initWithLatitude:37.677742 longitude:-120.976257], // Starbucks
			 [[CLLocation alloc] initWithLatitude:37.781738 longitude:-121.226738], // Bass Pro
			 [[CLLocation alloc] initWithLatitude:37.785876 longitude:-121.252965], // Chilaberries
			 [[CLLocation alloc] initWithLatitude:37.694602 longitude:-121.065226], // Best Buy
			 [[CLLocation alloc] initWithLatitude:37.688269 longitude:-121.053135], // Coach
			 [[CLLocation alloc] initWithLatitude:37.668800 longitude:-120.991409], // Keller's
			 [[CLLocation alloc] initWithLatitude:40.714352 longitude:-74.005973],  // Home
			 [[CLLocation alloc] initWithLatitude:37.660903 longitude:-121.012634], // At Home Restaurant
			 nil];
	
	spot_radii = [[NSArray alloc] initWithObjects:
				  [NSNumber numberWithInt:30],  // Starbucks
				  [NSNumber numberWithInt:152], // Bass Pro
				  [NSNumber numberWithInt:60],  // Chilaberries
				  [NSNumber numberWithInt:76],  // Best Buy
				  [NSNumber numberWithInt:305], // Coach
				  [NSNumber numberWithInt:152], // Keller's
				  [NSNumber numberWithInt:30],  // Home
				  [NSNumber numberWithInt:30],  // At Home Restaurant
				  nil];
	
	currentSpot = 1;
	
    [window makeKeyAndVisible];
    
    return YES;
}

- (void)batteryStateDidChange:(UIDeviceBatteryState*)state {
	if ([[UIDevice currentDevice] batteryState] == UIDeviceBatteryStateCharging) {
		[status setText:@"Charging"];
		// They just plugged in the charger so start the quest!
		[self startQuest];
	}
}

- (void)startQuestManually {
	[self startQuest];
}

- (void)startQuest {
	if (currentSpot == 1) {
		[self checkInAtSpot];
		[locationManager startUpdatingLocation];
	}
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation {
	[location setText: [newLocation description]];
	[currentTarget setText:[NSString stringWithFormat:@"Target: %d", currentSpot]];
	[accuracy setText:[NSString stringWithFormat:@"Accuracy: %f", [newLocation horizontalAccuracy]]];
	
	// Don't start calculating the distance because the first spot is out in front
	// of home where they'll receive instructions.
	if (currentSpot != 1) {
		float target = [[spots objectAtIndex:currentSpot-2] getDistanceFrom:newLocation];
		[distanceToTarget setText:[NSString stringWithFormat:@"Distance: %f", target]];
		if (target <= [[spot_radii objectAtIndex:currentSpot-2] intValue]) {
			[self checkInAtSpot];
		}
	} else {
		// Keep trying to check in to the first spot until the server responds ok.
		[self checkInAtSpot];
	}

}

- (void)checkInAtSpot {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://xmas-quest.heroku.com/spots/%d/check_in", currentSpot]];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url 
															cachePolicy:NSURLRequestReloadIgnoringCacheData 
																timeoutInterval:60];
	
	[request setHTTPMethod:@"POST"];
	
	if (!connectionInProgress) {
		connectionInProgress = [[NSURLConnection alloc] initWithRequest:request	
															   delegate:self startImmediately:YES];
		
		[responseData release];
		responseData = [[NSMutableData alloc] init];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	[checkInResponse setText:[NSString stringWithFormat:@"Response: %@", responseString]];
	
	if ([responseString isEqualToString:@"ok"]) {
		if ((currentSpot-1) < [spots count]) {
			currentSpot++;
		} else {
			[locationManager stopUpdatingLocation];
			[currentTarget setText:@"Quest Complete!"];
		}
	}
	
	[responseString release];
	[self resetConnection];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self resetConnection];
}

- (void)resetConnection {
	[connectionInProgress release];
	connectionInProgress = nil;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
	[status release];
	[location release];
	[distanceToTarget release];
	[currentTarget release];
	[checkInResponse release];
	[accuracy release];
	[locationManager release];
	[connectionInProgress release];
	[responseData release];
	[spots release];
	[spot_radii release];
  [window release];
  [super dealloc];
}


@end
