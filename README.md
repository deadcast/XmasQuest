XmasQuest
=========

XmasQuest is a high-tech scavenger hunt game that uses Heroku and Twilio. When people on your scavenger hunt reach certain destinations, they are called and a computer generated voice tells them the next clue. It's a cool mashup of voice technology and geolocation data!

To create a xmas quest, figure out all the places where people will have to go. Get the lat and lon points for each place and add them into the list in the iPhone app source code. Once those are added, edit the Rails app to connect to your Twilio account and then update/modify the call methods to say a certain message and whom to call when he/she/they reach a certain destination. An iPhone running the xmas software must be present with the people during the scavenger hunt. It is keeping track of where they are and initializing the clue calls. You can also have SMS messages get sent upon arrival of certain places.

Once you have the Rails app and the iPhone app all configured, compile the iPhone app and push it to your phone via xcode and then push the Rails app to your Heroku account. After you've done that...... let the hunt begin! Just give the people going on the scavenger hunt an iPhone with the xmas quest software running and that's it! You can keep up-to-date with the progress of the quest by visiting the main index of the Rails app.

I've also included the CAD file for the case which I printed using Ponoko's 3d printing ceramic service. It came out awesome! :)

Note
----

You need to have a delayed job worker on your Heroku account running in order for this all to work.